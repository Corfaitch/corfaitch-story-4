from django.urls import path, include
from .views import *

app_name = 'ppw'

urlpatterns = [
    path('', home, name='home'),
    path('story3', index, name='story3'),
    path('story3more', story, name='story3more')
]